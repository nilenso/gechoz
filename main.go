package main

import (
	"fmt"
	"math"
	http "net/http"
	_ "net/http/pprof"
	"os"
	"strconv"
	"time"

	zmq "github.com/pebbe/zmq4"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app         = kingpin.New("gechoz", "")
	quesPort    = app.Flag("qport", "Port to publish questions").Required().Int()
	ansPort     = app.Flag("aport", "Port to receive answers").Required().Int()
	playerCount = app.Flag("pcount", "Player count").Required().Int()
	quesCount   = app.Flag("qcount", "Question count").Default("10").Int()

	master  = app.Command("master", "Run the master")
	players = app.Command("players", "Run the players")
)

func main() {
	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case master.FullCommand():
		runMaster()

	case players.FullCommand():
		runPlayers()
	}
}

func runMaster() {
	go func() {
		fmt.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	zmq.SetMaxSockets(5000000)

	publisher, _ := zmq.NewSocket(zmq.PUB)
	defer publisher.Close()
	publisher.SetSndhwm(*playerCount)
	publisher.Bind(fmt.Sprintf("tcp://*:%d", *quesPort))

	sink, _ := zmq.NewSocket(zmq.PULL)
	defer sink.Close()
	sink.SetSndhwm(*playerCount)
	sink.Bind(fmt.Sprintf("tcp://*:%d", *ansPort))

	fmt.Println("Press any key to start ...")
	fmt.Scanln()

	start := time.Now()
	for i := 0; i < *quesCount; i++ {
		if _, err := publisher.Send(strconv.Itoa(i), 0); err != nil {
			panic(err)
		}
		fmt.Println("Sent question:", i)

		for j := 0; j < *playerCount; j++ {
			if _, err := sink.Recv(0); err != nil {
				panic(err)
			}
			// fmt.Println("Received answer:", j)
		}
	}
	elapsed := int(time.Now().Sub(start)) / 1000000 / *quesCount

	if _, err := publisher.Send("END", 0); err != nil {
		panic(err)
	}

	time.Sleep(time.Second)
	fmt.Println("Avg time per question: ", elapsed, "ms")
}

type Player struct {
	id     int
	qCount int
}

const WORKER_PLAYER_COUNT = 500

func runPlayers() {
	// go func() {
	// 	fmt.Println(http.ListenAndServe("localhost:6061", nil))
	// }()

	zmq.SetMaxSockets(2000000)

	workerCount := int(math.Ceil(float64(*playerCount) / float64(WORKER_PLAYER_COUNT)))
	receivers := make([]chan int, workerCount)

	for i := 0; i < workerCount; i++ {
		receivers[i] = make(chan int)
		pCount := int(math.Min(float64(*playerCount-i*WORKER_PLAYER_COUNT), WORKER_PLAYER_COUNT))
		go runWorker(i, receivers[i], pCount)
	}

	fmt.Println("Waiting for questions")

	for i, receiver := range receivers {
		<-receiver
		close(receiver)
		fmt.Println("Worker shut down:", i)
	}
}

func runWorker(workerId int, latch chan int, pCount int) {
	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect(fmt.Sprintf("tcp://localhost:%d", *quesPort))
	subscriber.SetSubscribe("")

	sender, _ := zmq.NewSocket(zmq.PUSH)
	defer sender.Close()
	sender.Connect(fmt.Sprintf("tcp://localhost:%d", *ansPort))

	players := make([]*Player, pCount)
	for i := 0; i < pCount; i++ {
		playerId := workerId*WORKER_PLAYER_COUNT + i
		players[i] = &Player{id: playerId}
	}

	for {
		q, _ := subscriber.Recv(0)
		if q == "END" {
			break
		} else {
			for i := 0; i < pCount; i++ {
				players[i].qCount++
				sender.Send(q, 0)
				// fmt.Println("Sent answer:", q, player.id)
			}
		}
	}

	latch <- 0
	fmt.Println("Worker shutting down:", workerId)
}
